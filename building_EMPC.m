%% Load data if only this script is supposed to run
% load("simulation_results_AE-MPC.mat")

%% define economic opt problem with imperfect parameters
params.const.P = compute_P(params);

% E-MPC setup
opt_2eco = struct();
opt_2eco = MPC_setup(opt_2eco, params);

%% Loop
% Initialize the simulation
w = zeros(params.sys.n,1);
x_2 = x_0;
u_2 = zeros(params.sys.m,1);
theta_0 = params.theta_0;
theta_LMS = params.theta_0;

% Initialize the storage arrays
x_arr_2 = [x_2];
u_arr_2 = [u_2];
LMS_arr = [theta_LMS];

% perform one system step to run paramater estimation (zero input)
w_old = [0;0];
[x_2,~, w] = step_true(x_2, u_2, params, 0, w_old);
x_arr_2 = [x_arr_2, x_2];
V_arr_2 = [0];
s_arr_2 = zeros(length(params.con.X.b));

% compute system matrices based on initial parameter
A_est = A_d0 + A_ddelta{1}*theta_0(1) + A_ddelta{2}*theta_0(2); 
B_est = B_d0 + B_ddelta{1}*theta_0(1) + B_ddelta{2}*theta_0(2);

fprintf("Loop running, progress:\t %i %%", round(1/(n_steps+1)*100))
for i=1:n_steps+1
    if mod(i,10) == 0
       fprintf(repmat('\b',1,numel(num2str(round(i/(n_steps+1)*100)))+2)); % Move the cursor back
       fprintf('%i %%', round(i/(n_steps+1)*100)); % Print the iteration number
    end
    
    p2 = {x_2, A_est, B_est, cost_pt};
    [u2,out2,val3] = solve_problem(p2, opt_2eco);
    V_arr_2 = [V_arr_2; out2{2}];
    s_arr_2 = [s_arr_2, out2{3}(:,1)];
    u_2 = u2(:,1);

    % propagate true system
    [x_2,~,~] = step_true(x_2,u_2,params, i, w_arr_saved);

    u_arr_2 = [u_arr_2,u_2];
    x_arr_2 = [x_arr_2,x_2];
end %for
fprintf("\n\nDone\n");
clear opt_2eco
save('simulation_results_AE-_E-MPC.mat');

%% figure (221)
figure(221);
t = tiledlayout(2,2, 'TileSpacing','Compact', 'Padding','tight');
%%% state 1
ax1=nexttile;
plot(linspace(0, length(x_arr)*10/60/24, length(x_arr)), x_arr(1,:),'LineWidth', 0.75 , 'DisplayName', 'AE-MPC');
grid on; hold on;
plot(linspace(0, length(x_arr_2)*10/60/24, length(x_arr_2)), x_arr_2(1,:), 'LineWidth', 0.75 ,'LineStyle','--', 'DisplayName','E-MPC');
plot([0,35], -params.con.X.b(1)*ones(2,1), 'k--', 'LineWidth', 1.5,'DisplayName','Limits')
ylabel('$[x]_1$', 'Interpreter','latex', 'FontSize', 20); 
alpha(1); set(gca,"FontSize",20);ylabel('$[x]_1$', 'Interpreter','latex', 'FontSize', 22);
%state
ax3=nexttile;
plot(linspace(0, length(x_arr)*10/60/24, length(x_arr)), x_arr(1,:),'LineWidth', 0.75 , 'DisplayName', 'AE-MPC');
grid on; hold on;
plot(linspace(0, length(x_arr_2)*10/60/24, length(x_arr_2)), x_arr_2(1,:), 'LineWidth', 1 ,'LineStyle','--', 'DisplayName','E-MPC');
plot([0,35], -params.con.X.b(1)*ones(2,1), 'k--', 'LineWidth', 1.5,'DisplayName','Limits')
alpha(1); set(gca,"FontSize",20);
%%% input
ax2=nexttile;
plot(linspace(0, length(u_arr)*10/60/24, length(u_arr)), u_arr(1,:),'-', 'LineWidth', 0.75 , 'DisplayName', 'AE-MPC');
alpha(0.5)
grid on; hold on;
alpha(0.5)
plot(linspace(0, length(u_arr_2)*10/60/24, length(u_arr_2)), u_arr_2(1,:), '--','LineWidth', 1 , 'DisplayName','E-MPC');
alpha(0.5)
plot([0,n_steps+1], [-u_s, -u_s], 'k--',  'LineWidth', 1.5,'DisplayName','Limits');
plot([0,n_steps+1], [u_s,u_s], 'k--',  'LineWidth', 1.5,'HandleVisibility','off')
alpha(0.1); 
linkaxes([ax1,ax2],'x'); xlim([0,3.5]);
set(gca,"FontSize",20); ylabel('$u$', 'Interpreter','latex', 'FontSize', 22);
%%% input
ax4=nexttile;
plot(linspace(0, length(u_arr)*10/60/24, length(u_arr)), u_arr(1,:),'LineWidth', 0.75 , 'DisplayName', 'AE-MPC');
alpha(0.5)
grid on; hold on;
alpha(0.5)
plot(linspace(0, length(u_arr_2)*10/60/24, length(u_arr_2)), u_arr_2(1,:), '--','LineWidth', 1 , 'DisplayName','E-MPC');
alpha(0.5)
plot([0,n_steps+1], [-u_s, -u_s], 'k--',  'LineWidth', 1.5,'DisplayName','Limits');
plot([0,n_steps+1], [u_s,u_s], 'k--',  'LineWidth', 1.5,'HandleVisibility','off')
alpha(0.1); 

linkaxes([ax3,ax4],'x'); xlim([20,23.5]); 
linkaxes([ax2,ax4],'y'); ylim(ax2,[-0.95, 0.85]);
linkaxes([ax1,ax3], 'y'); ylim(ax1, [-2.5,3.5]);
set(gca,"FontSize",18);legend("NumColumns",3, 'FontSize', 16, 'Location', 'southeast'); 
xlabel(t,'Time [days]', 'FontSize', 20, 'Interpreter', 'latex')
set(gcf,'position',[100,100,950,650])
% the axes are numered as follows: [ax1, ax3;
%                                   ax2, ax4]

figure(221); exportgraphics(gcf, 'figures/state_input_two_periods.eps');

%% difference in energy consumption (sum of input)
fprintf("\nThe AE-MPC schemed used %.4f energy units, ", sum(u_arr(1,:)+u_s));
fprintf("the economic MPC schemed used %.4f energy units.\n", sum(u_arr_2(1,:)+u_s));
fprintf("The difference (AE-MPC -- E-MPC) = %.4f\n", sum(u_arr(1,:)-u_arr_2(1,:)));
fprintf("This results in a percentwise change of %.4f %%.", sum(u_arr(1,:)+u_s)/sum(u_arr_2(1,:)+u_s)*100-100);

%% Performance plot

    %%% compute cost of AE-MPC
    cost_arr = zeros(1,length(x_arr));
    cost_cum_arr_nonav = zeros(1,length(x_arr));
    cost_tot = 0;
    % compute performance / economic costs
    Qeco = params.const.Q; Reco = params.const.R; qeco = params.const.q; reco = params.const.r;
    for j = 2:length(cost_arr)
        violation = s_arr(:,j-1);
        cost_step = x_arr(:,j-1)'*Qeco*x_arr(:,j-1) + u_arr(:,j-1)'*Reco*u_arr(:,j-1)+...
            qeco'*x_arr(:,j-1) + reco'*u_arr(:,j-1) + violation'*params.const.Lambda(1,1)*violation;
        cost_arr(j) = cost_step;
        cost_cum_arr_nonav(j) = cost_tot+cost_step;
        cost_tot = cost_tot + cost_step;
    end %for-j

% these values are recalled from the AE-MPC simulation
params.const.Qeco = params.const.Q;
params.const.Reco = params.const.R;
params.const.qeco = params.const.q;
params.const.reco = params.const.r;

%%%compute costs for A-MPC and E-MPC
% initialize arrays to store costs over time
cost_tot_2 = 0;
cost_arr_2 = zeros(1,length(x_arr));
cost_cum_arr_2 = zeros(1,length(x_arr));
cost_cum_arr_2nonav = zeros(1,length(x_arr));

% compute costs for each time step
for i=2:length(cost_arr_2)
    violation = s_arr_2(:,i-1);
    cost_step = x_arr_2(:,i-1)'*params.const.Q*x_arr_2(:,i-1) + u_arr_2(:,i-1)'*params.const.R*u_arr_2(:,i-1)+...
        params.const.q'*x_arr_2(:,i-1) + params.const.r'*u_arr_2(:,i-1) + violation'*params.const.Lambda(1,1)*violation;
    cost_arr_2(i) = cost_step;
    cost_cum_arr_2(i) = (cost_tot_2+cost_step)/i;
    cost_cum_arr_2nonav(i) = cost_tot_2+cost_step;
    cost_tot_2 =  cost_tot_2 + cost_step;
end

%% linear plot
figure(112);tiledlayout(1,1, 'TileSpacing','compact', 'Padding','tight'); ax1=nexttile;
hold off;
plot(linspace(0, length(cost_cum_arr_nonav)*10/60/24, length(cost_cum_arr_nonav)),cost_cum_arr_nonav, 'LineStyle','-',  'LineWidth',1);                    % AE-MPC
hold on; grid on;
plot(linspace(0, length(cost_cum_arr_2nonav)*10/60/24, length(cost_cum_arr_2nonav)),cost_cum_arr_2nonav, 'LineStyle','--', 'LineWidth',1.5)    % economic MPC
xlim([0,n_steps*1.1])
ylabel('Accumulated cost','interpreter','latex'); % xlabel('Time [days]');
ylim([min(cost_cum_arr_2nonav), max(cost_cum_arr_2nonav)*1.02])
set(gca, 'FontSize', 20); 
% ax2=nexttile;
% hold off;
% plot(linspace(0, length(cost_arr)*10/60/24, length(cost_arr)),cost_arr,  'LineStyle','-', 'LineWidth',1);                    % AE-MPC
% hold on; grid on;
% plot(linspace(0, length(cost_arr_2)*10/60/24, length(cost_arr_2)),cost_arr_2, 'LineStyle','--', 'LineWidth',1.5)    % economic MPC
% xlim([0,n_steps*1.1])
legend('AE-MPC', 'E-MPC', 'Location','northeast', 'Orientation','horizontal', 'Location','northwest'); 
% ylabel('Average cost','interpreter','latex')
xlabel('Time [days]', 'Interpreter', 'Latex'); 
linkaxes([ax1,ax2],'x'); xlim([0,params.const.n_steps*10/60/24*1.02]);
set(gca, 'FontSize', 20); 

set(gcf,'position',[100,100,950,300])
figure(112); exportgraphics(gcf, 'figures/performance_comparison.eps')

%% End of this script
fprintf("\nFinished E-MPC simulation script.\n")