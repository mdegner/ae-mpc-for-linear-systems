function [state,out,w] = step_true(x,u, params, k, w_traj)
    sys = params.sys;

    if k>0
        w =  w_traj(:,k);
        ret = params.noise.W.project(w);
        w = ret.x;
    else
        w = [0;0];
    end
    
    state = sys.true.A*x + sys.true.B*u + w;
    out = sys.C*state+sys.D*u;
end %step