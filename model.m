function params=model(params)
    %% recall parameters
    T_s = params.const.T_s;

    %%room with size 10x10 and height 3, wall thickness 0.2m and lambda =
    % 0.39W/K/m
    d_concrete = 0.15;       % [m]
    lambda_concrete = 0.06;% [W/(m*K)]
    d_PU = 0.02;             % [m]
    lambda_PU       = 0.025;% [W/(m*K)]
    
    % thermal resistance of the inner wall layer, i.e., between 
    % room air and layer interface in the wall
    R1 = d_concrete/(lambda_concrete*10*3*4*1e-3);     % [K/kW]

    % room heat capacity
    mr = (10*10*3)*1.225;     % 300m^3*1.225kg/m^3
    cpr = 1.000;    % 1kJ/(kg*K)
    
    % thermal resistance of the outer wall layer, i.e., between outside
    % air and the layer interface in the wall
    R2 = d_PU/(lambda_PU*10*3*4*1e-3);     % [K/kW]
    
    % lumped heat capacity of the wall
    mw = (10*3*4)*d_concrete*800;    % 24m^3*800kg/m^3
    cpw = 0.880;    % 0.88kJ/(kg*K)
    
    
    %%% select the scaling of the second parameter here
    scale = 100;
    params.const.theta_true = [1/R1;1/(scale*R2)];
    params.noise.mode = "outdoor-sine-both";
    
    % continuous time model (with time derivatives in 1/h and true parameters)
    A = [-1/(R1*cpr*mr),        1/(R1*cpr*mr);
         1/(R1*cpw*mw),     -1/(R2*mw*cpw)-1/(R1*cpw*mw)];
    
    B = [1/(cpr*mr);
         0];
    
    E = [0;
         1/(R2*cpw*mw)];
    
    % get system dimensions
    params.sys.n = size(A,2);
    params.sys.m = size(B,2);
    
    % output matrices for completeness
    C = eye(params.sys.n);
    D = zeros(params.sys.n, params.sys.m);
    params.sys.C = C;
    params.sys.D = D;
    
    A_0 = zeros(2);
    A_delta{1} = [-1/(cpr*mr),        1/(cpr*mr);
                  1/(cpw*mw),     -1/(cpw*mw)];
    A_delta{2} = [0,      0;
                  0,     -1*scale/(cpw*mw)];
    
    B_0 = [1/(cpr*mr);0];
    B_delta{1} = zeros(params.sys.n, params.sys.m);
    B_delta{2} = zeros(params.sys.n, params.sys.m);
    
    A_d0 = eye(2);
    A_ddelta{1} = A_delta{1}*T_s;
    A_ddelta{2} = A_delta{2}*T_s;
    B_d0 = B_0*T_s;
    B_ddelta{1} = B_delta{1}*T_s;
    B_ddelta{2} = B_delta{2}*T_s;
    
    A_dt = (eye(2)+T_s*A);
    B_dt = T_s*B;
    E_dt = T_s*E;
    
    
    params.sys.A_ddelta = A_ddelta;
    params.sys.B_ddelta = B_ddelta;
    params.sys.A_0 = A_d0;
    params.sys.B_0 = B_d0;
    params.sys.true.A = A_dt;
    params.sys.true.B = B_dt;
    params.sys.true.E = E_dt;
    params.sys.p = length(A_ddelta);

end %function