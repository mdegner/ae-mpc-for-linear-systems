function [u,out,info] = solve_problem(parameters, obj)
%SOLVE_PROBLEM takes the Yalmip optimization problem formulated using
%"optimizer" and input parameters and returns the solution
    inputs=parameters;
    [sol, errcode] = obj.prob(inputs);
    % check for errors
    if errcode
        info = yalmiperror(errcode);
        disp(info);
    else
        info = false;
    end
    % parse optimizer outputs
    if iscell(sol)
        u = sol{1};
        out = {sol{2:end}};
    else
        u = sol;
        out = [];
    end
end %solve_problem