function [dist_traj, params] = create_disturbances(params, show_plot)
    %CREATE_DISTURBANCES Creates a disturbances trajectory

    % recall values from the true system matrices
    n_steps = params.const.n_steps;
    noise_base= params.noise.noise_base;
    temp_base = params.noise.temp_base;
    true_E = params.sys.true.E(2); 
    true_B = params.sys.true.B(1);
    
    % Disturbance polytope W and computation of the disturbance trajectory 
    
    params.noise.A_w = [1,0;-1,0; 0, 1; 0,-1];
    params.noise.b_w = [noise_base*true_B;noise_base*true_B; temp_base*true_E; temp_base*true_E];
    params.noise.W1 = Polyhedron(params.noise.A_w, params.noise.b_w);
    dist_traj = zeros(2,n_steps+10);
    for i = 2:n_steps+10
        % uniform disturbances
        % w1 = true_B*noise_base*random('Uniform',-1.5,1.5);% (sin(2*pi/(24*6)*i)+random('Uniform',-0.5,0.5));
        % w2 = true_E*temp_base*random('Uniform', -1, 1);%(sin((2*pi/(24*6))*i-0.4)+random('Uniform', -1, 1));
        
        % % sinus-based disturbances
        w1 = true_B*noise_base*(sin(2*pi/(24*6)*i)+random('Uniform',-0.5,0.5));
        w2 = true_E*temp_base*(sin((2*pi/(24*6))*(i-9))+random('Uniform', -1, 1));
        ret = params.noise.W.project([w1;w2]);
        
        % no exponential decay, could be added here
        dist_traj(:,i) = ret.x;
    end
    
    if show_plot
        figure(99);t=tiledlayout(2,1);nexttile;
        plot(dist_traj(1,:)/true_B);
        xlim([0,350]); ylabel('w_1');
        nexttile; plot(dist_traj(2,:)/true_E)
        xlim([0,350]); ylabel('w_2'); xlabel('Time k []')
        title(t,'Part of the created disturbance trajectory')
    end
end

