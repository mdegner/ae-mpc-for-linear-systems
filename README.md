# Adaptive Economic MPC for linear systems

## Description
This repository contains the simulation code corresponding to the publication "Adaptive Economic Model Predictive Control for linear systems with performance guarantees". The MATLAB code simulates a simple one-temperature zone thermal model of a building subject to parametric uncertainty and bounded disturbances. One file implements the AE-MPC scheme ([main_example.m](main_example.m)), the other file ([building_EMPC.m](building_EMPC.m)) implements an economic MPC scheme without parameter adaptation. The results are shown in various diagrams.

A sample result file is provided ([simulation_results_AE-_E-MPC.mat](simulation_results_AE-_E-MPC.mat)) and can be used to obtain that are used in the publication (also contained in the [figures](figures) folder). Furthermore, the disturbance trajectory that was used in the publication is provided ([disturbance_trajectory.mat](disturbance_trajectory.mat)).

## Successfully running the code
To run the provided code successfully on your machine, we recommend to use MATLAB R2023b with MOSEK Version 10.1.21, YALMIP Version 20230622, and with the MPT3 toolbox (http://control.ee.ethz.ch/~mpt).

Start the simulation by running the [main_example.m](main_example.m) file.

## Authors and acknowledgment
This code is supplementary material to a conference paper authored by:
Maximilian Degner (mdegner[AT]ethz.ch), Raffaele Soloperto, Melanie Zeilinger, John Lygeros, Johannes Köhler.

## License
See [license file](LICENSE) (MIT License).