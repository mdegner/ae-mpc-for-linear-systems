function [] = plot_basic(par)
    %PLOT_BASIC creates five different plots for the simulated system
    
    % Recall values
    x_arr = par.stored.x_arr;
    w_arr = par.stored.w_arr;
    u_arr = par.stored.u_arr;
    LMS_arr = par.stored.LMS_arr;
    n_steps = par.const.n_steps;
    u_s = par.sys.u_s;

    % close all
    % figure(1);
    % plot(x_arr(1,:),x_arr(2,:));
    % xlabel('room temp'); ylabel('wall temp')
    % grid on;
    % exportgraphics(gcf,'figures/State-Space.pdf');
    
    figure(2);t = tiledlayout(2,1);ax1=nexttile;
    plot(x_arr(1,:), 'k');
    grid on; hold on;
    ylabel('\Delta T_r   [K]'); title('room temp')
    plot([0,n_steps+2], par.con.X.b(1)*ones(2,1), 'k--')
    % plot([0,n_steps+2], -par.con.X.b(2)*ones(2,1), 'k--')
    legend('State trajectory', 'Limits of polytopic constraint set')
    ax2=nexttile;
    plot(x_arr(2,:));
    grid on;
    ylabel('\Delta T_w   [K]'); title('wall temp');
    linkaxes([ax1,ax2],'x'); xlim([0,n_steps*1.05])
    xlabel('Time [10mins]')
    title(t, "State vs time for AE-MPC")
    % exportgraphics(gcf, 'figures/State-vs-time.pdf')
    
    figure(3);t = tiledlayout(1,1);nexttile;
    plot(u_arr(1,:));
    grid on; hold on;
    plot([0,n_steps+1], [-u_s, -u_s], 'k--', [0,n_steps+1], [u_s,u_s], 'k--');
    legend('Input u', 'Input limits')
    ylabel('\Delta u  [kW]'); xlabel('Time [10mins]')
    title('heat flow from heater into room (deviation from u_s = '+string(round(u_s,3))+'kW)')
    title(t, "Input vs time for AE-MPC")
    % exportgraphics(gcf, 'figures/Input-vs-time.pdf')
    
    figure(4);t = tiledlayout(1,2, "TileSpacing","compact", 'Padding','tight');
    len_LMS = length(LMS_arr);
    ax1=nexttile;hold off;
    plot(linspace(0, length(LMS_arr)*10/60/24, length(LMS_arr)), LMS_arr(1,:), 'k', 'LineWidth',1.25);
    hold on; grid on;
    plot([0, n_steps], [par.Theta.V(1,1), par.Theta.V(1,1)], 'k--',  'LineWidth',1.5)
    plot([0, n_steps], [par.Theta.V(3,1), par.Theta.V(3,1)], 'k--', 'LineWidth',1.5)
    plot(linspace(0,len_LMS,len_LMS), ones(len_LMS,1)*par.const.theta_true(1), 'b:',  'LineWidth',2);
    %lim = max(par.const.theta_true(1)*1.5, max(LMS_arr(1,:))); grid on; 
    ylim([-0.004147,0.154253]); % ylim([0.0, par.Theta.V(1,1)*1.1]);
    % legend('LMS estimate', 'True parameter')
    ylabel('Estimate of $[\theta]_1$', 'Interpreter','latex');xlabel('Time [days]', 'Interpreter','latex')
    set(gca, 'FontSize', 20); 
    ax2=nexttile; hold off;
    plot(linspace(0, length(LMS_arr)*10/60/24, length(LMS_arr)), LMS_arr(2,:), 'k', 'LineWidth',1.25);
    hold on; grid on;
    plot([0, n_steps], [par.Theta.V(1,2), par.Theta.V(1,2)], 'k--', 'LineWidth',1.5);
    plot([0, n_steps], [par.Theta.V(2,2), par.Theta.V(2,2)], 'k--', 'LineWidth',1.5);
    plot(linspace(0,len_LMS,len_LMS), ones(len_LMS,1)*par.const.theta_true(2), 'b:', 'LineWidth',2);
    %lim = max(par.const.theta_true(2)*1.5, max(LMS_arr(2,:))); grid on; ylim([-0.05, lim])
    ylabel('Estimate of $[\theta]_2$', 'Interpreter','latex'); 
    % legend('LMS estimate', 'True parameter','Location','best')
    ylim([-0.0003,0.0082]); %ylim([0, par.Theta.V(1,2)*1.1])
    linkaxes([ax1,ax2],'x'); xlim([0,length(LMS_arr)*10/60/24*1.05]);
    xlabel('Time [days]', 'Interpreter','latex')
    set(gca, 'FontSize', 20); 
    set(gcf,'position',[300,300,950,400])
    % title(t, "Parameter estimates vs time for AE-MPC")
    exportgraphics(gcf, 'figures/LMS-vs-time.eps')
    
    figure(5);t = tiledlayout(2,1);ax1=nexttile;
    plot(linspace(0, length(LMS_arr)*10/60/24, length(LMS_arr)),w_arr(1,:)./par.sys.true.B(1));
    grid on; ylabel('\Delta q_{sol}   [kW]')
    title('"disturbance" heat flow (e.g., solar irradiation, open windows, humans)')
    ax2=nexttile;
     plot(linspace(0, length(LMS_arr)*10/60/24, length(LMS_arr)),w_arr(2,:)./par.sys.true.E(2));
    grid on;
    title('disturbance of the outdoor temperature (deviation from T_o=0°C)'); ylabel('\Delta T_o   [K]')
    linkaxes([ax1,ax2],'x'); % xlim([0,n_steps*1.05])
    xlabel('Time [days]');
    title(t, "Disturbances vs time (same for AE-MPC and E-MPC)")
    % exportgraphics(gcf, 'figures/disturbances-vs-time.pdf')   
    
    fprintf("\n");
end %function