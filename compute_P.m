function P = compute_P(param_struct)
    %COMPUTE_TUBE Computes tube & terminal controller and terminal
    %cost. The method is adapted from Appendix A in Köhler et al. (2019),
    %"Linear robust adaptive model predictive control:
    %Computational complexity and conservatism"
    %doi: 10.48550/arXiv.1909.01813
    
    %%% Parse inputs %%%
    switch nargin
        case 1
            
        otherwise
            error('Wrong number of inputs!')
    end
       
    sys = param_struct.sys;
    const = param_struct.const;

    % define verteces of Omega, this is \theta^j in Köhler2020 (Elisa
    % paper)
    vertices_theta = param_struct.Theta.V';
    
    % look up necessary values
    A_delta = sys.A_ddelta;
    B_delta = sys.B_ddelta;
    n = sys.n;
    m = sys.m;
    Q = const.Q_bar;
    R = const.R;
    if any(R,"all")
        disp("Caution: R not equal zero.")
    end

    W = Polyhedron(param_struct.noise.A_w, param_struct.noise.b_w);
    
    % Computation of a rho-contractive Polytope
    % Find feedback K and terminal cost P
    E = sdpvar(n,n);        % = P^-1
    Y = zeros(m,n);      % = K*P^-1 = 0 (since we do not have a tube controller)
    
    % objective
    objective = -logdet(E);
    
    % constraints
    constraints=[];
    for i=1:size(vertices_theta,1)
        A = sys.A_0;
        for j=1:length(A_delta)
            A = A + A_delta{j}*vertices_theta(i,j);
        end %for length(A_delta)
        
        B = sys.B_0;
        for j=1:length(B_delta)
            B = B + B_delta{j}*vertices_theta(i,j);
        end %for length(B_delta)

        % Lyapunov equation
        constraints=[constraints,...
            [E, (A*E+B*Y)', Q^0.5*E, R^0.5*Y';
            (A*E+B*Y), E, zeros(n), zeros(n,m);
            (Q^0.5*E)', zeros(n), eye(n), zeros(n,m);
            (R^0.5*Y')', zeros(m,n), zeros(m,n), eye(m)]>=0];
    end %for size(vertices_theta)
    
    % solve
    optimize(constraints, objective, sdpsettings('verbose', 0));
    P = inv(value(E));
end %function