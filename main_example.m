%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The following code is supplementary material to the paper 
% "Adaptive Economic Model Predictive Control for linear systems with performance 
% guarantees", by Maximilian Degner, Raffaele Soloperto, Melanie N. 
% Zeilinger, John Lygeros, Johannes Köhler. The paper was submitted on
% March 22, 2024 to IEEE's CDC 2024 conference.
%
% This code is provided under the MIT license as stated in the file LICENSE
% in the repository
% https://gitlab.ethz.ch/mdegner/ae-mpc-for-linear-systems.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Clean up
clear all; close all; clc;

%% Disturbance definition
params.const.T_s = 600;             % sampling time of the discrete-time model, unit [s]
mu = 0.00005;
n_steps = 50*(24*6);                 % days * (time steps per day)
params.const.n_steps = n_steps;

params.const.N = 18;                % horizon of the MPC
params.const.mu = mu;               


% the base values dictate the size of the disturbances (if a new trajectory
% is created) and the size of the disturbance set W
params.noise.noise_base = 0.595;
params.noise.temp_base = 7;

%% Get disturbance trajectory
% load disturbance trajectory
load("disturbance_trajectory.mat", "w_arr_saved", "W_set");
params.noise.W = W_set; 
params.noise.A_w = W_set.A; params.noise.b_w = W_set.b; clear W_set;
% % use this line to create a new disturbance trajectory, if no file
% % w_arr.mat exists in the workspace
% w_arr = create_disturbances(params, false);
% w_arr_saved = w_arr; W_set = params.noise.W;
% save("disturbance_trajectory.mat", "w_arr_saved", "W_set");

%% Get model, define parameteric uncertainty and check for stability
params = model(params);

% recall variables
A_dt = params.sys.true.A; B_dt = params.sys.true.B; 
A_d0 = params.sys.A_0; A_ddelta = params.sys.A_ddelta;
B_d0 = params.sys.B_0; B_ddelta = params.sys.B_ddelta;

% compute steady state for physical temperatures, get nominal heat flow u_s
T_oe = 0; T_re = 22;        % outdoor temperature 0°C, room temperature 22°C
T_we = (A_dt(2,1)*T_re+params.sys.true.E(2)*T_oe)/(1-A_dt(2,2));
u_e  = (T_re*(1-A_dt(1,1))-A_dt(1,2)*T_we)/B_dt(1);
x_s = [T_re;T_we];
u_s = u_e;
w_s = T_oe;
params.sys.x_s = x_s; params.sys.u_s = u_s; params.sys.w_s = w_s;

% parameter set Theta, initial parameter estimate
A_theta = [1,0;-1,0;0,1;0,-1];
b_theta = [params.const.theta_true(1)*3; -params.const.theta_true(1)*0.1; params.const.theta_true(2)*5; -params.const.theta_true(2)*0.3];
params.Theta = Polyhedron(A_theta, b_theta);
params.theta_0 = [0.01;0.005];
params.theta_0 = [0.1;b_theta(3)];

% check A stable for all parameter values
theta_vert = params.Theta.V;
for i = 1:length(b_theta)
    A_check = params.sys.A_0 + params.sys.A_ddelta{1}*theta_vert(i,1) + ...
        params.sys.A_ddelta{2}*theta_vert(i,2);
    e = eig(A_check);
    if any(abs(e)>1)
        error("A(theta) not stable")
    end
end

params.const.Q = diag([500,0]); 
params.const.R = 0;
params.const.q = [0;0]; 
params.const.r = 600;     %.approx 17 Rappen/ kWh
params.const.u_s = u_s;
params.const.x_s = x_s;


%% Setup soft constraints, compute terminal cost, and setup MPC problem
params.const.Lambda = 1e5; X_A = [-1]; X_b = [1];
params.con.X = Polyhedron(X_A,X_b);

% matrix Q_bar is used for the terminal cost as
params.const.Q_bar = params.const.Q + params.con.X.A'*params.const.Lambda*params.con.X.A;

% Offline computations: Terminal cost
params.const.P = compute_P(params);

fprintf("AE-MPC: Finished setting up the model and computed P.\n")
fprintf("\n\nP = ");disp(params.const.P)

% Setup optimization problem with Yalmip
opt = struct();
opt = MPC_setup(opt, params);

%% Initialization of the simulation
% initialize system at steady state
x_0 = [0;0];                    % initial state
x = x_0;                        % initial state
w = zeros(params.sys.n,1);      % initial disturbance
u = zeros(params.sys.m, 1);     % initial input
theta_LMS = params.theta_0;     % initial parameter estimate

% initialize storage arrays
x_arr = [x_0];
u_arr = [zeros(params.sys.m, 1)];
LMS_arr = [theta_LMS];
w_arr = [0;0];

% perform one system step to run paramater estimation (zero input)
[x,~, w] = step_true(x, u, params, 0, w_arr(:,1));
x_arr = [x_arr, x];
V_arr = [0];
s_arr = zeros(length(params.con.X.b));


%% Loop running up to n_steps
tic;        % Record computation time
fprintf("Running AE-MPC simulation: Loop running, progress:\t %i %%", round(1/(n_steps+1)*100))
for i=1:n_steps+1
    % Progress update in the console output
    if mod(i,20) == 0
       fprintf(repmat('\b',1,numel(num2str(round(i/(n_steps+1)*100)))+2)); % Move the cursor back
       fprintf('%i %%', round(i/(n_steps+1)*100)); % Print the iteration number
    end

    %%% Estimate parameters: First line is the one using D_k*(\theta^\ast -
    % \theta_k), second line is the one using x_{k+1}-x_{1|k}
    [theta_LMS, ~] = param_estim(x_arr(:,end-1), u_arr(:,end), x_arr(:,end), theta_LMS, params.Theta, params); 
    LMS_arr = [LMS_arr, theta_LMS];  

    % computation of parameter-dependent system matrices
    A_est = A_d0 + A_ddelta{1}*theta_LMS(1) + A_ddelta{2}*theta_LMS(2); 
    B_est = B_d0 + B_ddelta{1}*theta_LMS(1) + B_ddelta{2}*theta_LMS(2);
    if any(params.const.q)
        cost_pt = params.const.q'*inv(eye(params.sys.n)-A_est);
    else
        cost_pt = zeros(size(params.const.q))';
    end

    %%% solve MPC
    p = {x, A_est, B_est, cost_pt};
    [u,out,val] = solve_problem(p, opt);
    V_arr = [V_arr; out{2}];
    s_arr = [s_arr, out{3}(:,1)];
    u = u(:,1);                         % save first optimal input

    %%% propagate true system
    [x,~,w] = step_true(x,u,params, i, w_arr_saved);%  takes an old w value and returns x_k+1 and w_k
    u_arr = [u_arr,u];
    x_arr = [x_arr,x];

    w_arr = [w_arr,w];
end %for
toc;        % Record computation time

fprintf("\n--DONE--\n");

% Save storage arrays to params that will be used for plotting later
params.stored.x_arr = x_arr;
params.stored.u_arr = u_arr;
params.stored.w_arr = w_arr;
params.stored.LMS_arr = LMS_arr;
params.stored.V_arr = V_arr;
%% call plotting function
plot_basic(params);     % five plots of system evolution, not all are usable in publications

% save workspace to file
clear opt
save('simulation_results_AE-MPC.mat');

% Call the script that runs the non-adaptive economic MPC simulation with
% the same disturbances
building_EMPC;

%% End of simulation
fprintf("\nEnd of the script.\n")

