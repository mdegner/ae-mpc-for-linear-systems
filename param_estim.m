function [theta_hat, theta_nom] = param_estim_2(x,u,x_next, theta, uncert_set, params)
%PARAM_ESTIM Compute LMS and nominal parameter estimates
% [theta_hat, theta_nom] = param_estim(x,u,w,theta, uncert_set, params)

% Recall constants
mu = params.const.mu;
theta_true = params.const.theta_true;
p = length(params.const.theta_true);

A_ddelta = params.sys.A_ddelta;
B_ddelta = params.sys.B_ddelta;

% Compute D_k, A_theta, B_theta
D_k = zeros(params.sys.n, p);
A_theta = params.sys.A_0;
B_theta = params.sys.B_0;
for i=1:p
    D_k(:,i) = A_ddelta{i}*x+B_ddelta{i}*u;
    A_theta = A_theta + A_ddelta{i}*theta(i);
    B_theta = B_theta + B_ddelta{i}*theta(i);
end %for

%% Point estimates 
%%% LMS update equations for best estimate

% LMS update equation
% test = D_k*(theta_true-theta);
%%% temp = x_{k+1} - x_{1|k} = x_{k+1} - (  A(\theta_k)*x_{k} + B(\theta_k)*u_{k}  )
temp = x_next - (A_theta*x+B_theta*u);
theta_tilde = theta + mu*D_k'*(temp);

% disp(eig(D_k'*D_k));
% disp(svd(D_k*diag([1,100])));

% LMS projection
ret = uncert_set.project(theta_tilde);
theta_hat = ret.x; ok=ret.exitflag; dist=ret.dist;
if ok == 0
    error('Projection of LMS estimate failed')
end %if


%%% Nominal parameter as center of hyperbox
%%% only for set-membership
% outBox = uncert_set.outerApprox;
% theta_nom = sum(outBox.A.*outBox.b)'/2;
theta_nom = [0;0];

end %function