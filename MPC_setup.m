function obj = MPC_setup(obj, par)
% x_0, A_theta,B_theta, pt, params
    obj.x = sdpvar(par.sys.n, par.const.N+1,'full');         % state
    obj.u = sdpvar(par.sys.m, par.const.N,'full');           % control
    
    % soft constraints
    obj.s = sdpvar(length(par.con.X.b), par.const.N, 'full');
    % parameters
    obj.A_theta = sdpvar(par.sys.n, par.sys.n, 'full');
    obj.B_theta = sdpvar(par.sys.n, par.sys.m, 'full');
    obj.x_0 = sdpvar(par.sys.n, 1, 'full');
    obj.pt = sdpvar(1, par.sys.n, 'full');

    % recall variables
    P = par.const.P;
    u_s = par.const.u_s;
    
    objective = 0; 
    constraints = [obj.x(:,1)==obj.x_0];
    
    for i=1:par.const.N
        constraints = [constraints; (obj.x(:,i+1) == obj.A_theta*obj.x(:,i)+obj.B_theta*obj.u(:,i)): "model constraint"];
        % stage cost
        objective=objective + obj.x(:,i)'*par.const.Q*obj.x(:,i) + obj.u(:,i)'*par.const.R*obj.u(:,i)+...
            par.const.q'*obj.x(:,i) + par.const.r'*obj.u(:,i) + obj.s(:,i)'*par.const.Lambda*obj.s(:,i);
    
        constraints = [constraints; (-obj.x(1,i) <=1+obj.s(1,i)): "temperature soft constraint"];
        constraints = [constraints; (obj.s(:,i) >= zeros(length(par.con.X.b),1)): "slack larger zero"];
        constraints = [constraints; (obj.u(i) >= -u_s): "input lower bound"];
        constraints = [constraints; (obj.u(i) <= u_s): "input upper bound"];
    end %for

    objective = objective + obj.pt*obj.x(:,end) + obj.x(:,end)'*P*obj.x(:,end);
    % With the soft constraints, there is no compact terminal set, it is R^n

    settings = sdpsettings('verbose',0,'solver','mosek');
    input_param = {obj.x_0, obj.A_theta, obj.B_theta, obj.pt};
    obj.prob=optimizer(constraints, objective, settings, input_param, {obj.u, obj.x, objective, obj.s});

end %function